#include "game_defs.hpp"

#if (USE_SFML == 1)

#include "sfml_layer.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <fstream>
#include <sstream>
#include <rapidxml.hpp>
#include <map>
#include "game_layer.hpp"

void KeyPressed(const sf::Event& event, sf::Keyboard::Key key, KeyState* out_key_state);
void KeyReleased(const sf::Event& event, sf::Keyboard::Key key, KeyState* out_key_state);

/// <summary>
/// Draw a sprite.
/// </summary>
/// <param name="ptr_draw_component">Sprite component which needs to be cast to DrawComponent.</param>
/// <param name="source_rect">Source rectangle from texture to draw.</param>
/// <param name="x">x position</param>
/// <param name="y">y position</param>
/// <param name="w">width</param>
/// <param name="h">height</param>
/// <param name="rotation">rotation</param>
void DrawSprite(void* sf_sprite, Rect* source_rect, I32 x, I32 y, U32 w, U32 h, F32 rotation);


static sf::RenderWindow* ptr_window = nullptr;
static sf::Texture* ptr_game_sprite_sheet_texture;
static sf::Texture* ptr_background_texture;
static sf::Texture* ptr_explosion_texture;
static std::map<std::string, DrawComponent> draw_component_map;
static DrawComponent background_draw_component;
static DrawComponent explosion_draw_component;


// Set it to outside code.
GameUpdate* ptr_cb_game_update = nullptr;
GameRender* ptr_cb_game_render = nullptr;

void SFML_Init()
{
	ptr_cb_game_update = UpdateGame;
	ptr_cb_game_render = RenderGame;

	// create the window
	ptr_window = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Space Shooter by Luka");
}

void SFML_LoadAssets()
{
	// Load background
	ptr_background_texture = new sf::Texture();
	ptr_background_texture->loadFromFile(PATH_TO_BACKGROUND "purple.png");

	background_draw_component.sprite = sf::Sprite(*ptr_background_texture);

	// Load explosion
	ptr_explosion_texture = new sf::Texture();
	ptr_explosion_texture->loadFromFile(PATH_TO_EXPLOSIONS "exp.png");

	explosion_draw_component.sprite = sf::Sprite(*ptr_explosion_texture);

	// Load the sprite sheet 
	ptr_game_sprite_sheet_texture = new sf::Texture();
	ptr_game_sprite_sheet_texture->loadFromFile(PATH_TO_GAME_ART_SHEET "sheet.png");

	// Open xml file which describes sprite sheet
	std::fstream xml_fstream(PATH_TO_GAME_ART_SHEET "sheet.xml", std::fstream::in);

	// Read the contents into string buffer, in order to pass it to xml_document
	std::stringstream buffer;
	buffer << xml_fstream.rdbuf();

	// Must do content_string variable, to avoid dangling pointer, as string should be in scope.
	std::string content_string = buffer.str();
	const char* content = content_string.c_str();

	// Load string into xml_document. 
	// NOTE: game_sprite_sheet_xml is heap allocated and should be deleted at end.
	rapidxml::xml_document<>* game_sprite_sheet_xml = new rapidxml::xml_document<>;
	game_sprite_sheet_xml->parse<0>(const_cast<char*>(content));

	// Get the subtexture node, which contains name of texture, x,y, width and height.
	rapidxml::xml_node<>* sub_texture_node = game_sprite_sheet_xml->first_node()->first_node();
	while (sub_texture_node)
	{
		std::string name_attrib = sub_texture_node->first_attribute("name")->value();
		const int x = std::stoi(sub_texture_node->first_attribute("x")->value());
		const int y = std::stoi(sub_texture_node->first_attribute("y")->value());
		const int w = std::stoi(sub_texture_node->first_attribute("width")->value());
		const int h = std::stoi(sub_texture_node->first_attribute("height")->value());

		// every 'name' attribute is png, therefore has suffix '.png'. Remove it for map, to simpilfy it.
		std::string key = name_attrib.substr(0, name_attrib.length() - 4);
		DrawComponent draw_component;
		draw_component.sprite = sf::Sprite(*ptr_game_sprite_sheet_texture, { x,y,w,h });
		draw_component_map.emplace(key, draw_component);

		sub_texture_node = sub_texture_node->next_sibling();
	}

	// Release temporary allocated resources.
	delete game_sprite_sheet_xml;
	buffer.clear();
	xml_fstream.close();
}

void SFML_Run()
{
	GameUpdateInput game_input;

	// to get delta time 
	sf::Clock clock;
	float prev_time = 0.0f;


	// run the program as long as the window is open
	while (ptr_window->isOpen())
	{
#pragma region UPDATE
		float current_time = clock.getElapsedTime().asMilliseconds();
		game_input.DeltaTime = current_time - prev_time;
		prev_time = current_time;

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (ptr_window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				KeyPressed(event, sf::Keyboard::A, &game_input.KeyboardEvents.A_KeyDown);
				KeyPressed(event, sf::Keyboard::S, &game_input.KeyboardEvents.S_KeyDown);
				KeyPressed(event, sf::Keyboard::D, &game_input.KeyboardEvents.D_KeyDown);
				KeyPressed(event, sf::Keyboard::W, &game_input.KeyboardEvents.W_KeyDown);
				KeyPressed(event, sf::Keyboard::Up, &game_input.KeyboardEvents.Up_KeyDown);
				KeyPressed(event, sf::Keyboard::Down, &game_input.KeyboardEvents.Down_KeyDown);
				KeyPressed(event, sf::Keyboard::Left, &game_input.KeyboardEvents.Left_KeyDown);
				KeyPressed(event, sf::Keyboard::Right, &game_input.KeyboardEvents.Right_KeyDown);
				KeyPressed(event, sf::Keyboard::Space, &game_input.KeyboardEvents.Space_KeyDown);
				break;
			case sf::Event::KeyReleased:
				KeyReleased(event, sf::Keyboard::A, &game_input.KeyboardEvents.A_KeyDown);
				KeyReleased(event, sf::Keyboard::S, &game_input.KeyboardEvents.S_KeyDown);
				KeyReleased(event, sf::Keyboard::D, &game_input.KeyboardEvents.D_KeyDown);
				KeyReleased(event, sf::Keyboard::W, &game_input.KeyboardEvents.W_KeyDown);
				KeyReleased(event, sf::Keyboard::Up, &game_input.KeyboardEvents.Up_KeyDown);
				KeyReleased(event, sf::Keyboard::Down, &game_input.KeyboardEvents.Down_KeyDown);
				KeyReleased(event, sf::Keyboard::Left, &game_input.KeyboardEvents.Left_KeyDown);
				KeyReleased(event, sf::Keyboard::Right, &game_input.KeyboardEvents.Right_KeyDown);
				KeyReleased(event, sf::Keyboard::Space, &game_input.KeyboardEvents.Space_KeyDown);
				break;
				// "close requested" event: we close the window
			case sf::Event::Closed:
				ptr_window->close();
				break;
			}
		}

		// NOTE: this calls into game layer.
		ptr_cb_game_update(game_input);
#pragma endregion


#pragma region DRAW
		// clear the window with black color
		ptr_window->clear(sf::Color::Black);

		// Call the ptr to GAME_UPDATE and pass DrawSprite function to it.
		ptr_cb_game_render(DrawSprite);

		// end the current frame
		ptr_window->display();
#pragma endregion
	}
}

void SFML_Destroy()
{
	delete ptr_background_texture;
	delete ptr_game_sprite_sheet_texture;
	delete ptr_window;
}


DrawComponent* GetDrawComponent(const char* name)
{
	DrawComponent* result = &draw_component_map.at(name);
	return result;
}

DrawComponent* GetBackgroundDrawComponent()
{
	DrawComponent* result = &background_draw_component;
	return result;
}

DrawComponent* GetExplosionDrawComponent()
{
	DrawComponent* result = &explosion_draw_component;
	return result;
}

void KeyPressed(const sf::Event& event, sf::Keyboard::Key key, KeyState* out_key_state)
{
	if (event.key.code == key)
	{
		*out_key_state = KeyState::Pressed;
	}
}

void KeyReleased(const sf::Event& event, sf::Keyboard::Key key, KeyState* out_key_state)
{
	if (event.key.code == key)
	{
		*out_key_state = KeyState::Released;
	}
}

void DrawSprite(void* ptr_draw_component, Rect* source_rect, I32 x, I32 y, U32 w, U32 h, F32 angle)
{
	DrawComponent& draw_component = *static_cast<DrawComponent*>(ptr_draw_component);

	sf::Sprite& sprite = draw_component.sprite;

	// there is no information about scale, but sf::Sprite needs one, therefore derive it.
	F32 scale_x = w / sprite.getLocalBounds().width;
	F32 scale_y = h / sprite.getLocalBounds().height;

	if (source_rect)
	{
		sprite.setTextureRect({ source_rect->X, source_rect->Y, source_rect->W, source_rect->H });
	}

	F32 half_w = w / 2;
	F32 half_h = h / 2;

	// Find middle of sprite for rotation, which is just width/height halfed divided by our scale.
	F32 rotation_x_origin = half_w / scale_x;
	F32 rotation_y_origin = half_h / scale_y;

	// offset position a bit, since origin is moved to half a sprite.
	sprite.setPosition(x + half_w, y  + half_h);
	sprite.setOrigin(rotation_x_origin, rotation_y_origin);
	sprite.setRotation(Degrees(angle));
	sprite.setScale(scale_x, scale_y);

	ptr_window->draw(sprite);

#ifdef DEBUG
	if (draw_component.DEBUG_draw_collision_rect)
	{
		sf::RectangleShape shape;
		shape.setPosition(x, y);
		shape.setSize(sf::Vector2f(w, h));

		// set the shape color to green
		shape.setFillColor(sf::Color(255, 0, 0, 128));

		ptr_window->draw(shape);
	}
#endif 
}


#endif