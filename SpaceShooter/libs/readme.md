# Libs

Currently SFML library is used.

Folder structure is as follows, unless platform agnostic ( header only files )

{OS}-{architecture}-{package} for example
{Win}-{64}-{SFML-2.5.1} where Win stands for Windows, 64 for 64 bit architecture and SFML is package with version.

Win - for Windows project is compiled only using cl compiler ( Visual C++ )