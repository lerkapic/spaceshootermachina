#pragma once

#ifndef bones_math_H

#define bones_math_H 

#include "game_defs.hpp"
#include <math.h>
#include <cstdlib>


// NOTE: move all of this to library, so that it can be reused between projects.

namespace bns
{
#define PI 3.14159274101257324f
#define HALF_PI (PI * 0.5f)
#define TWO_PI (PI * 2.0f)

	inline F32 Sqrt(F32 val)
	{
		// TODO: improve
		F32 result = sqrt(val);
		return result;
	}

	/// <summary>
	/// Returns a random number, between two given numbers.
	/// </summary>
	/// <param name="min">Min random number.</param>
/// <param name="max">Max random number, not including one passed in. Meaning if 4 is passed it's never going to be above 3.</param>
	/// <returns>Random number.</returns>
	inline I32 Random(I32 min, I32 max)
	{
		I32 value = max - min;
		I32 result = rand() % value + min;
		return result;
	}

	/// <summary>
	/// Returns a random number, between two given numbers.
	/// </summary>
	/// <param name="min">Min random number.</param>
	/// <param name="max">Max random number, not including one passed in. Meaning if 4 is passed it's never going to be above 3.</param>
	///// <returns>Random number.</returns>
	inline F32 Random(F32 min, F32 max)
	{
		F32 value = max - min;
		// value between 0.0f and 1.0f
		F32 random = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
		// move to correct space
		F32 result = random * value + min;
		return result;
	}

	/// <summary>
	/// Clamps value between min and max and returns it.
	/// </summary>
	/// <param name="min">Min value to clamp to.</param>
	/// <param name="max">Max value to clamp to.</param>
	/// <param name="val">Value to clamp.</param>
	/// <returns>Return value.</returns>
	inline F32 Clamp(F32 min, F32 max, F32 val)
	{
		if (val < min)
		{
			val = min;
			return val;
		}
		else if (val > max)
		{
			val = max;
			return val;
		}
		return val;
	}

	/// <summary>
	/// Clamps value between min and max.
	/// </summary>
	/// <param name="min">Min value to clamp to.</param>
	/// <param name="max">Max value to clamp to.</param>
	/// <param name="out_val">Value to clamp.</param>
	inline void Clamp(F32 min, F32 max, F32* out_val)
	{
		*out_val = Clamp(min, max, *out_val);
	}

	/// <summary>
	/// Get the sine of an angle value.
	/// </summary>
	/// <param name="theta">Angle theta in radians.</param>
	inline F32 Sin(F32 theta)
	{
		F32 result = sinf(theta);
		return result;
	}

	/// <summary>
	/// Get the cos of an angle value.
	/// </summary>
	/// <param name="theta">Angle theta in radians.</param>
	inline F32 Cos(F32 theta)
	{
		F32 result = cosf(theta);
		return result;
	}

	/// <summary>
	/// Converts degrees to radians
	/// </summary>
	inline F32 Radians(F32 degrees)
	{
		F32 result = (degrees * PI) / 180;
		return result;
	}

	/// <summary>
	/// Converts radians to degrees 
	/// </summary>
	inline F32 Degrees(F32 radians)
	{
		F32 result = (radians * 180) / PI;
		return result;
	}

	/// <summary>
	/// Returns the value of the arc tangent of y/x, expressed in radians.
	/// </summary>
	inline F32 Atan2(F32 y, F32 x)
	{
		F32 result = atan2f(y, x);
		return result;
	}

	/// <summary>
	/// Gets the absolute value of a number.
	/// </summary>
	inline F32 Abs(F32 value)
	{
		F32 result = abs(value);
		return result;
	}
}
/*
	NOTES: magnitude/length of vector is same thing, but depends on context. If vector is line segment one can ask for it's length, if vector is representing a physical quantity
		   such as acceleration or velocity one ask for it's magnitude.
*/

namespace bns
{
	/// <summary>
	/// The Vec2 struct, which componenxt X and Y are of F32 
	/// </summary>
	struct Vec2f
	{
		F32 X;
		F32 Y;

		/// <summary>
		/// Default constructor.
		/// Initializes properties to 0.
		/// </summary>
		Vec2f()
			: X(0.0f), Y(0.0f) {}

		/// <summary>
		/// Vec2 constructor.
		/// </summary>
		/// <param name="x">The X component.</param>
		/// <param name="y">The Y component.</param>
		Vec2f(F32 x, F32 y) :
			X(x), Y(y) {}

		/// <summary>
		/// Get the component by index.
		/// </summary>
		inline F32 operator[](U32 index)
		{
			// Get pointer to x and increase by index, then dereference
			F32 result = *(&this->X + index);

			return result;
		}

		/// <summary>
		/// Set length of vector to 0.
		/// </summary>
		inline void SetLengthToZero()
		{
			X = 0;
			Y = 0;
		}

		/// <summary>
		/// Set the magnitude of vector to 0.
		/// </summary>
		inline void SetMagnitudeToZero()
		{
			SetLengthToZero();
		}

		/// <summary>
		/// Set the length of a vector.
		/// </summary>
		inline void SetLength(F32 length)
		{
			Normalize();
			X *= length;
			Y *= length;
		}

		/// <summary>
		/// Set the magnitude of a vector.
		/// </summary>
		inline void SetMagnitude(F32 magnitude)
		{
			SetLength(magnitude);
		}

		/// <summary>
		/// Get length or magnitude of a vector.
		/// </summary>
		inline F32 Length()
		{
			F32 result = X * X + Y * Y;
			result = Sqrt(result);
			return result;
		}

		/// <summary>
		/// Get the magnitude of a vector.
		/// </summary>
		/// <returns></returns>
		inline F32 Magnitude()
		{
			F32 result = Length();
			return result;
		}

		/// <summary>
		/// Clamps the upper bound of length of a vector to a given length.
		/// </summary>
		/// <param name="max_length">Length to clamp to, if current length is greater then given.</param>
		inline void ClampToMaxLength(F32 max_length)
		{
			// Sqaure root is slower, therefore just use squared one.
			F32 current_length = LengthSquared();
			if (current_length > max_length * max_length)
			{
				SetLength(max_length);
			}
		}

		/// <summary>
		/// Clamps the upper bound of magniture of a vector to a given magniture.
		/// </summary>
		/// <param name="max_length">Magnitude to clamp to, if current magniture is greater then given.</param>
		inline void ClampToMaxMagnitude(F32 max_magnitude)
		{
			ClampToMaxLength(max_magnitude);
		}

		/// <summary>
		/// Normalite a vector. Sets it's length or magnitude to 1.
		/// </summary>
		inline void Normalize()
		{
			F32 l = Length();
			if (l > 0)
			{
				X /= l;
				Y /= l;
			}
			else
			{
				SetLengthToZero();
			}
		}

		/// <summary>
		/// Gets the squared length of vector.
		/// </summary>
		/// <returns>Squared length of vector.</returns>
		inline F32 LengthSquared() const
		{
			F32 result = X * X + Y * Y;
			return result;
		}

		/// <summary>
		/// Returns the angle of a vector, in radians.
		/// </summary>
		inline F32 Angle() const
		{
			F32 result = Atan2(Y, X);
			return result;
		}

		/// <summary>
		/// The direction of a vector, in radians.
		/// </summary>
		inline F32 Direction() const
		{
			F32 result = Angle();
			return result;
		}

		/// <summary>
		/// Rotate the current vector.
		/// </summary>
		/// <param name="angle">Angle to rotate by.</param>
		inline void Rotate(F32 angle)
		{
			F32 new_x = X * Cos(angle) - Y * Sin(angle);
			F32 new_y = X * Sin(angle) + Y * Cos(angle);
			X = new_x;
			Y = new_y;
		}

		/// <summary>
		/// Adds amount to current angle.
		/// </summary>
		/// <param name="angle_amount_to_add">Amount to add in radians.</param>
		inline void AddToAngle(F32 angle_amount_to_add)
		{
			F32 current_angle = Angle() + angle_amount_to_add;
			Rotate(current_angle);
		}

		/// <summary>
		/// Add amount to current direction vector.
		/// </summary>
		/// <param name="direction_amount_to_add">Amound to add in radians.</param>
		inline void AddToDirection(F32 direction_amount_to_add)
		{
			AddToAngle(direction_amount_to_add);
		}

		/// <summary>
		/// Adds vectors component wise.
		/// </summary>
		inline Vec2f& operator+=(const Vec2f& rhs)
		{
			X += rhs.X;
			Y += rhs.Y;
			return *this;
		}


		/// <summary>
		/// Adds vectors components wise.
		/// </summary>
		inline friend Vec2f operator +(Vec2f lhs, const Vec2f& rhs)
		{
			lhs += rhs;
			return lhs;
		}

		/// <summary>
		/// Subtracts vectors component wise.
		/// </summary>
		inline Vec2f& operator-=(const Vec2f& rhs)
		{
			X -= rhs.X;
			Y -= rhs.Y;
			return *this;
		}

		/// <summary>
		/// Subtracts vectors components wise.
		/// </summary>
		inline friend Vec2f operator -(Vec2f lhs, const Vec2f& rhs)
		{
			lhs -= rhs;
			return lhs;
		}

		/// <summary>
		/// Multiplication by scalar.
		/// </summary>
		inline Vec2f& operator *=(const F32 scalar)
		{
			X *= scalar;
			Y *= scalar;
			return *this;
		}

		/// <summary>
		/// Multiplication by scalar.
		/// </summary>
		inline friend Vec2f operator* (Vec2f lhs, const F32 scalar)
		{
			lhs *= scalar;
			return lhs;
		}

		/// <summary>
		/// Multiplication by scalar.
		/// </summary>
		inline friend Vec2f operator* (F32 s, Vec2f v)
		{
			v.X *= s;
			v.Y *= s;
			return v;
		}

		/// <summary>
		/// Gets the zero vector.
		/// </summary>
		inline static Vec2f Zero()
		{
			Vec2f result = Vec2f(0, 0);
			return result;
		}

		/// <summary>
		/// Create the vector from angle in radians.
		/// </summary>
		inline static Vec2f FromAngle(F32 angle)
		{
			Vec2f result = { Cos(angle), Sin(angle) };
			return result;
		}

		/// <summary>
		/// Creates vector from direction in radians.
		/// </summary>
		inline static Vec2f FromDirection(F32 direction)
		{
			Vec2f result = FromAngle(direction);
			return result;
		}
	};

	/// <summary>
	/// Structure for representing a simple point.
	/// </summary>
	struct Point2i
	{
		U32 X;
		U32 Y;
	};
}
#endif 