#include "bones_behaviour_systems.hpp"

void bns::SeekTarget(Vec2f current_position, Vec2f current_velocity, Vec2f target, F32 desired_vector_max_magnitude, F32 steer_max_magnitude, F32 dt, Vec2f& out_seeker_acceleration)
{
	// Calculate the desired velocity to target at max speed.
	Vec2f desired = target - current_position;
	desired.ClampToMaxMagnitude(desired_vector_max_magnitude);

	// Formulat for steering force
	Vec2f steer = desired - current_velocity;
	steer.ClampToMaxMagnitude(steer_max_magnitude);
	out_seeker_acceleration += steer * dt;
}

void bns::SeekTarget(Vec2f current_position, Vec2f current_velocity, Vec2f target, F32 dt, Vec2f& out_seeker_acceleration)
{
	SeekTarget(current_position, current_velocity, target, 0.3f, 0.1f, dt, out_seeker_acceleration);
}