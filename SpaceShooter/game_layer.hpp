#pragma once

#ifndef GAME_LAYER_H

#define GAME_LAYER_H

#include "bones_math.hpp"

using namespace bns;

enum class KeyState
{
	None,
	Pressed,
	Released
};

struct KeyboardEvents
{
	KeyState Left_KeyDown;
	KeyState Right_KeyDown;
	KeyState Up_KeyDown;
	KeyState Down_KeyDown;

	KeyState A_KeyDown;
	KeyState D_KeyDown;
	KeyState W_KeyDown;
	KeyState S_KeyDown;

	KeyState Space_KeyDown;
};

struct GameUpdateInput
{
	float DeltaTime;
	KeyboardEvents KeyboardEvents;
};

#define GAME_UPDATE(name) void name(GameUpdateInput game_update_input)

/******* GAME_RENDER *************/
// Macro to create a render function of a given name, which takes callback as argument.
// Callback has a drawable component as a parameter, which is defined by platform above and is pointer to SDL, SFML or some other drawable component.
// Rect *source_rect is used to draw a portion of a texture if required
// x - x position
// y - y position
// w - width
// h - height
// rotation - rotation angle
#define GAME_RENDER(name) void name(void(*cb_draw_function)(void* drawable, Rect *source_rect, I32 x, I32 y, U32 w, U32 h, F32 angle))

struct Transform
{
	/// <summary>
	/// Positions at which to draw.
	/// </summary>
	Vec2f Position;

	// TODO: this might be for physics as well.
	/// <summary>
	/// Size of player sprite in pixels
	/// </summary>
	Vec2f Size;
};

struct Drawable : public Transform
{

	/// <summary>
	/// Pointer to whatever is supposed to draw component
	///  ( DrawComponent which holds sf::Sprite in case of SFML, DrawComponent which holds SDL_Texture in case of SDL ) 
	/// </summary>
	void* Drawable;
};

struct Player : Drawable
{
	// TODO: physics components, so move at one poit
	Vec2f Velocity;
	Vec2f Acceleration;
	F32 Radius;
	F32 Angle;

	// If it's less then 0, player can shoot.
	F32 ShootingCooldown;
};

struct Enemy : Drawable
{
	// TODO: physics components, so move at one point
	Vec2f Velocity;
	Vec2f Acceleration;
	F32 Radius;
	F32 Angle;

	// If it's less then 0, enemy can shoot.
	F32 ShootingCooldown;

	bool Active;
};

struct Bullet : Drawable
{
	// TODO: physics components, so move at one poit
	Vec2f Velocity;
	Vec2f Acceleration;
	F32 Radius;

	bool Active;
};

struct Background : Drawable
{
};

struct Explosion : Drawable
{
	U32 TotalAnimationRows;
	U32 TotalAnimationColumns;

	// Used for animation purposes, in order to be able to correctly select subsection of animation texture.
	Vec2f RealSize;

	// row of explosion sprite sheet which is currently playing.
	U32 AnimationRow;
	// column of explosion sprite sheet which is currently playing.
	U32 AnimationColumn;

	// When to advance to next step of animation
	F32 AnimationNextStepCooldown;

	bool Active;
};

struct Camera
{
	Vec2f Position;
	Vec2f Offset;
};

void InitPlayer(void* drawable, F32 size_x, F32 size_y, F32 radius);
void InitEnemies(void* drawable, F32 size_x, F32 size_y, F32 radius);
void InitPlayerBullets(void* drawable, F32 size_x, F32 size_y, F32 radius);
void InitEnemyBullets(void* drawable, F32 size_x, F32 size_y, F32 radius);
void InitBackground(void* drawable);
void InitExplosions(void* drawable, F32 size_x, F32 size_y, U32 TotalAnimationRows, U32 TotalAnimationColumns, U32 real_size_x, U32 real_size_y);

/// <summary>
/// defines function in which game code should reside
/// and a function which needs to be called by platform layer ( SFML, SDL, native )
/// </summary>
typedef GAME_UPDATE(GameUpdate);

/// <summary>
/// defines function in which game code should reside
/// and a function which needs to be called by platform layer ( SFML, SDL, native )
/// </summary>
typedef GAME_RENDER(GameRender);

/// <summary>
/// Updates the game. 
/// This code is usually called by whoever provides the game loop.
/// </summary>
/// <param name="dt">The delta time.</param>
GAME_UPDATE(UpdateGame);

/// <summary>
/// Called into whne game is to be renderer.
/// This code is usually called by whoever provides the render loop.
/// </summary>
GAME_RENDER(RenderGame);

#endif // !GAME_LAYER_H
