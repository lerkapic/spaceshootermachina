#pragma once

#ifndef GAME_PHYSICS_H

#define GAME_PHYSICS_H

#include "game_defs.hpp"
#include "bones_math.hpp"

using namespace bns;

/****************************** GAME PHYSICS ********************************/
/*
*	Contains all of the logic regarding physics of a game, such as movement and collision logic.
*/

/// <summary>
/// Simple methods that updates position and velocity over time according to simplified laws of motion.
/// </summary>
/// <param name="in_outr_current_position">The current position which is updated to new position.</param>
/// <param name="in_out_current_velocity">The current velocity which is updated to new velocity.</param>
/// <param name="acceleration">The acceleration.</param>
/// <param name="friction_factor">The friction to apply. The friction factor should be number close to 1 to work correctly, such as 0.995f for example. 
/// If 1.0f is passed, there is no friction applied.</param>
/// <param name="dt">DeltaTime</param>
void UpdateMotion(Vec2f& in_out_current_position, Vec2f& in_out_current_velocity, Vec2f acceleration, F32 dt, F32 friction_factor);

/// <summary>
/// Simple methods that updates position and velocity over time according to simplified laws of motion with no friction applied.
/// </summary>
/// <param name="in_outr_current_position">The current position which is updated to new position.</param>
/// <param name="in_out_current_velocity">The current velocity which is updated to new velocity.</param>
/// <param name="acceleration">The acceleration.</param>
/// <param name="dt">DeltaTime</param>
void UpdateMotion(Vec2f& in_out_current_position, Vec2f& in_out_current_velocity, Vec2f acceleration, F32 dt);

/// <summary>
/// The axis aligned box collision between two rectangles. Returns true if colliding/overlapping.
/// </summary>
bool AxisAlignedBoundingBoxCollision(Rect a, Rect b);

#endif // !GAME_PHYSICS_H
