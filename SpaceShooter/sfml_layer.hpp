#pragma once

#ifndef SFML_LAYER

#define SFML_LAYER

#include "game_defs.hpp"

#if (USE_SFML == 1)

#include "SFML/Graphics.hpp"

/// <summary>
/// Create struct that represents draw component.
/// </summary>
struct DrawComponent
{
	sf::Sprite sprite;

#ifdef DEBUG
	// use this region to set whatever might help with debugging
	bool DEBUG_draw_collision_rect;
#endif 
};

/// <summary>
/// Initialize the SFML stuff.
/// </summary>
void SFML_Init();

/// <summary>
/// Load the assets for SFML.
/// This is mainly loading textures and sounds.
/// </summary>
void SFML_LoadAssets();

/// <summary>
/// Run the SFML update and render loop.
/// </summary>
void SFML_Run();

/// <summary>
/// Destroy all the resources created for an SFML layer code.
/// </summary>
void SFML_Destroy();

/// <summary>
/// Gets the pointer to an sfml sprite.
/// Every sprite is internally loaded to map of { string , sf:Sprite }
/// </summary>
/// <param name="name">Name of sprite.</param>
/// <returns>Pointer to sf::Sprite</returns>
DrawComponent* GetDrawComponent(const char* name);

/// <summary>
/// Gets the pointer to an sfml sprite that represents background.
/// </summary>
/// <returns>Pointer to sf::Sprite</returns>
DrawComponent* GetBackgroundDrawComponent();

/// <summary>
/// Gets the pointer to an draw component that contains sfml components necessary for drawing.
/// </summary>
/// <returns>Pointer to draw component.</returns>
DrawComponent* GetExplosionDrawComponent();

#endif // USE_SFML

#endif // !SFML_LAYER
