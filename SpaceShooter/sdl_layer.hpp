#pragma once

#ifndef SDL_LAYER

#define SDL_LAYER

#include "game_defs.hpp"

#if (USE_SFML == 0)

#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_render.h>

/// <summary>
/// Create struct that represents draw component.
/// </summary>
struct DrawComponent
{
	SDL_Rect source_rect;
	SDL_Texture* ptr_texture;
#ifdef DEBUG
	// use this region to set whatever might help with debugging
	bool debug;
#endif 

};

/// <summary>
/// Initialize the SDL stuff.
/// </summary>
void SDL_Init();

/// <summary>
/// Load the assets for SDL.
/// This is mainly loading textures and sounds.
/// </summary>
void SDL_LoadAssets();

/// <summary>
/// Run the SDL update and render loop.
/// </summary>
void SDL_Run();

/// <summary>
/// Destroy all the resources created for an SDL layer code.
/// </summary>
void SDL_Destroy();

/// <summary>
/// Gets the pointer to an draw component.
/// Every component is internally loaded to map of { string , DrawComponent }
/// </summary>
/// <param name="name">Name of component.</param>
/// <returns>Pointer to draw component which holds information about SDL_Texture to draw and source rectangle.</returns>
DrawComponent* GetDrawComponent(const char* name);

/// <summary>
/// Gets the pointer to an background draw component, which contains contextal ( SFML, SDL other stuff ) data about draw component..
/// </summary>
/// <returns>Pointer to draw component which holds information about SDL_Texture to draw and source rectangle.</returns>
DrawComponent* GetBackgroundDrawComponent();

/// <summary>
/// Gets the pointer to an explosion draw component, which contains contextal ( SFML, SDL other stuff ) data about draw component.
/// </summary>
/// <returns>Pointer to draw component which holds information about SDL_Texture to draw and source rectangle.</returns>
DrawComponent* GetExplosionDrawComponent();

#endif // !USE_SFML

#endif // !SDL_LAYER
