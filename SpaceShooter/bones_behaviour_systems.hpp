#pragma once

#ifndef BNS_BEHAVIOUR_SYSTEM_H

#define BNS_BEHAVIOUR_SYSTEM_H

/*
	Various behaviour systems
	Reference material: https://natureofcode.com/book/chapter-6-autonomous-agents/
*/

#include "bones_math.hpp"

namespace bns
{
	/// <summary>
	/// Calculates the new acceleration for whoever is seeking a target.
	/// Assumes that standard equations of motions are used, where acceleration is added to velocity, and velocit is added to previous position.
	/// Such as p' = p + v + a
	/// Where p' is new position, p is current position, v is velocity and a is acceleration ( which is modified here ).
	/// Of course, example above is simplified.
	/// </summary>
	/// <param name="current_position">The current position of whoever is seeking target.</param>
	/// <param name="current_velocity">The current velocity of whoever is seeking target.</param>
	/// <param name="target">The target and desired position.</param>
	/// <param name="desired_vector_max_magnitude">The max magnitude of desired vector, which is t (target ) - p (position ).</param>
	/// <param name="steer_max_magnitude">The max magnitude of steering.</param>
	/// <param name="dt">The delta time.</param>
	/// <param name="out_seeker_acceleration">The acceleration to which new steering vector is added to.</param>
	void SeekTarget(Vec2f current_position, Vec2f current_velocity, Vec2f target, F32 desired_vector_max_magnitude, F32 steer_max_magnitude, F32 dt, Vec2f& out_seeker_acceleration);

	/// <summary>
	/// Calculates the new acceleration for whoever is seeking a target.
	/// Assumes that standard equations of motions are used, where acceleration is added to velocity, and velocit is added to previous position.
	/// Such as p' = p + v + a
	/// Where p' is new position, p is current position, v is velocity and a is acceleration ( which is modified here ).
	/// Of course, example above is simplified.
	/// This one is shorter version and has by default desired_vector_max_magnitude set to 0.1f and steer_max_magnitude set to 0.1f.
	/// </summary>
	/// <param name="current_position">The current position of whoever is seeking target.</param>
	/// <param name="current_velocity">The current velocity of whoever is seeking target.</param>
	/// <param name="target">The target and desired position.</param>
	/// <param name="dt">The delta time.</param>
	/// <param name="out_seeker_acceleration">The acceleration to which new steering vector is added to.</param>
	void SeekTarget(Vec2f current_position, Vec2f current_velocity, Vec2f target, F32 dt, Vec2f& out_seeker_acceleration);
}

#endif // !BNS_BEHAVIOUR_SYSTEM_H
