#include "game_defs.hpp"

#if (USE_SFML == 0)

#include "sdl_layer.hpp"
#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <fstream>
#include <sstream>
#include <rapidxml.hpp>
#include <map>
#include "game_layer.hpp"

void KeyPressed(const SDL_Event& event, SDL_Keycode key, KeyState* out_key_state);
void KeyReleased(const SDL_Event& event, SDL_Keycode key, KeyState* out_key_state);
/// <summary>
/// Draw a sprite.
/// </summary>
/// <param name="ptr_draw_component">Sprite component which needs to be cast to DrawComponent.</param>
/// <param name="source_rect">Source rectangle from texture to draw.</param>
/// <param name="x">x position</param>
/// <param name="y">y position</param>
/// <param name="w">width</param>
/// <param name="h">height</param>
/// <param name="rotation">rotation</param>
void DrawSprite(void* ptr_draw_component, Rect* source_rect, I32 x, I32 y, U32 w, U32 h, F32 rotation);

static SDL_Window* ptr_window = nullptr;
static SDL_Renderer* ptr_renderer;

static SDL_Surface* ptr_game_sprite_sheet_surface;
static SDL_Texture* ptr_game_sprite_sheet_texture;
static SDL_Surface* ptr_background_surface;
static SDL_Surface* ptr_explosion_surface;

static std::map<std::string, DrawComponent> draw_component_map;
static DrawComponent background_draw_component;
static DrawComponent explosion_draw_component;

static bool game_is_running = true;

// Set it to outside code.
GameUpdate* ptr_cb_game_update = nullptr;
GameRender* ptr_cb_game_render = nullptr;

// TODO: return error code if creation is not success.
void SDL_Init()
{
	ptr_cb_game_update = UpdateGame;
	ptr_cb_game_render = RenderGame;

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

	// create the window
	ptr_window = SDL_CreateWindow("Space Shooter by Luka", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);

	if (ptr_window == NULL)
	{
		// In the case that the window could not be made...
		printf("Could not create window: %s\n", SDL_GetError());
	}

	ptr_renderer = SDL_CreateRenderer(ptr_window, -1, SDL_RENDERER_ACCELERATED);

	if (ptr_renderer == NULL)
	{
		// In the case that the window could not be made...
		printf("Could not create window: %s\n", SDL_GetError());
	}
}

void SDL_LoadAssets()
{
	int flags = IMG_INIT_PNG;
	int initted = IMG_Init(flags);
	if ((initted & flags) != flags) {
		printf("IMG_Init: Failed to init required png support!\n");
		printf("IMG_Init: %s\n", IMG_GetError());
		// handle error
	}

	// Load background
	ptr_background_surface = IMG_Load(PATH_TO_BACKGROUND "purple.png");
	if (!ptr_background_surface)
	{
		printf("IMG_Load: %s\n", IMG_GetError());
	}

	background_draw_component.ptr_texture = SDL_CreateTextureFromSurface(ptr_renderer, ptr_background_surface);
	background_draw_component.source_rect = { 0,0, ptr_background_surface->w, ptr_background_surface->h };

	// Load explosion
	ptr_explosion_surface = IMG_Load(PATH_TO_EXPLOSIONS "exp.png");
	if (!ptr_explosion_surface)
	{
		printf("IMG_Load: %s\n", IMG_GetError());
	}

	explosion_draw_component.ptr_texture = SDL_CreateTextureFromSurface(ptr_renderer, ptr_explosion_surface);
	explosion_draw_component.source_rect = { 0,0, 64, 64 };

	// Load the sprite sheet 
	ptr_game_sprite_sheet_surface = IMG_Load(PATH_TO_GAME_ART_SHEET "sheet.png");
	if (!ptr_game_sprite_sheet_surface)
	{
		printf("IMG_Load: %s\n", IMG_GetError());
	}

	ptr_game_sprite_sheet_texture = SDL_CreateTextureFromSurface(ptr_renderer, ptr_game_sprite_sheet_surface);

	// Open xml file which describes sprite sheet
	std::fstream xml_fstream(PATH_TO_GAME_ART_SHEET "sheet.xml", std::fstream::in);

	// Read the contents into string buffer, in order to pass it to xml_document
	std::stringstream buffer;
	buffer << xml_fstream.rdbuf();

	// Must do content_string variable, to avoid dangling pointer, as string should be in scope.
	std::string content_string = buffer.str();
	const char* content = content_string.c_str();

	// Load string into xml_document. 
	// NOTE: game_sprite_sheet_xml is heap allocated and should be deleted at end.
	rapidxml::xml_document<>* game_sprite_sheet_xml = new rapidxml::xml_document<>;
	game_sprite_sheet_xml->parse<0>(const_cast<char*>(content));

	// Get the subtexture node, which contains name of texture, x,y, width and height.
	rapidxml::xml_node<>* sub_texture_node = game_sprite_sheet_xml->first_node()->first_node();
	while (sub_texture_node)
	{
		std::string name_attrib = sub_texture_node->first_attribute("name")->value();
		const int x = std::stoi(sub_texture_node->first_attribute("x")->value());
		const int y = std::stoi(sub_texture_node->first_attribute("y")->value());
		const int w = std::stoi(sub_texture_node->first_attribute("width")->value());
		const int h = std::stoi(sub_texture_node->first_attribute("height")->value());

		// every 'name' attribute is png, therefore has suffix '.png'. Remove it for map, to simpilfy it.
		std::string key = name_attrib.substr(0, name_attrib.length() - 4);
		DrawComponent component;
		component.ptr_texture = ptr_game_sprite_sheet_texture;
		component.source_rect.x = x;
		component.source_rect.y = y;
		component.source_rect.w = w;
		component.source_rect.h = h;
		draw_component_map.emplace(key, component);

		sub_texture_node = sub_texture_node->next_sibling();
	}

	// Release temporary allocated resources.
	delete game_sprite_sheet_xml;
	buffer.clear();
	xml_fstream.close();
}

void SDL_Run()
{
	/********************* START HERE ***************************/

	GameUpdateInput game_input;

	Uint32 prev_time = 0;

	// if not emscripten, since emscripten will use JavaScript request animation frame to loop.
#ifndef __EMSCRIPTEN__
	while (game_is_running)
#endif // !__EMSCRIPTEN__
	{
#pragma region UPDATE

		// if compiled with EMSCRIPTEN for WASM, code is handled by requestAnimationFrame and there is no need for delay
#ifndef __EMSCRIPTEN__

		// Get the current clock time 
		Uint32 current_time = SDL_GetTicks();

		// Get the delta time, time passed from last frame.
		game_input.DeltaTime = current_time - prev_time;

		// delay if necessary.
		int timeToWait = MS_PER_FRAME - (current_time - prev_time);
		if (timeToWait > 0 && timeToWait <= MS_PER_FRAME)
		{
			SDL_Delay(timeToWait);
		}

		// set up the last frame time.
		prev_time = current_time;
#else 

		// in case of emscripten, animation loop is handled by requestAnimationFrame, so just set this to MS_PER_FRAME to match 60fps. Default.
		game_input.DeltaTime = MS_PER_FRAME;
#endif //!__EMSCRIPTEN__



		// check all the window's events that were triggered since the last iteration of the loop
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_KEYDOWN:
				KeyPressed(event, SDLK_a, &game_input.KeyboardEvents.A_KeyDown);
				KeyPressed(event, SDLK_s, &game_input.KeyboardEvents.S_KeyDown);
				KeyPressed(event, SDLK_d, &game_input.KeyboardEvents.D_KeyDown);
				KeyPressed(event, SDLK_w, &game_input.KeyboardEvents.W_KeyDown);
				KeyPressed(event, SDLK_UP, &game_input.KeyboardEvents.Up_KeyDown);
				KeyPressed(event, SDLK_DOWN, &game_input.KeyboardEvents.Down_KeyDown);
				KeyPressed(event, SDLK_LEFT, &game_input.KeyboardEvents.Left_KeyDown);
				KeyPressed(event, SDLK_RIGHT, &game_input.KeyboardEvents.Right_KeyDown);
				KeyPressed(event, SDLK_SPACE, &game_input.KeyboardEvents.Space_KeyDown);
				break;
			case SDL_KEYUP:
				KeyReleased(event, SDLK_a, &game_input.KeyboardEvents.A_KeyDown);
				KeyReleased(event, SDLK_s, &game_input.KeyboardEvents.S_KeyDown);
				KeyReleased(event, SDLK_d, &game_input.KeyboardEvents.D_KeyDown);
				KeyReleased(event, SDLK_w, &game_input.KeyboardEvents.W_KeyDown);
				KeyReleased(event, SDLK_UP, &game_input.KeyboardEvents.Up_KeyDown);
				KeyReleased(event, SDLK_DOWN, &game_input.KeyboardEvents.Down_KeyDown);
				KeyReleased(event, SDLK_LEFT, &game_input.KeyboardEvents.Left_KeyDown);
				KeyReleased(event, SDLK_RIGHT, &game_input.KeyboardEvents.Right_KeyDown);
				KeyReleased(event, SDLK_SPACE, &game_input.KeyboardEvents.Space_KeyDown);
				break;
				// "close requested" event: we close the window
			case SDL_QUIT:
				game_is_running = false;
				break;
			}
		}

		// NOTE: this calls into game layer.
		ptr_cb_game_update(game_input);
#pragma endregion


#pragma region DRAW
		SDL_RenderClear(ptr_renderer); //clears the renderer


		// Call the ptr to GAME_UPDATE and pass DrawSprite function to it.
		ptr_cb_game_render(DrawSprite);

		// end the current frame
		SDL_RenderPresent(ptr_renderer);
#pragma endregion
	}
}

void SDL_Destroy()
{
	// Close and destroy the window
	SDL_DestroyTexture(ptr_game_sprite_sheet_texture);
	for (auto it = draw_component_map.begin(); it != draw_component_map.end(); ++it)
	{
		SDL_DestroyTexture(it->second.ptr_texture);
	}
	SDL_FreeSurface(ptr_background_surface);
	SDL_FreeSurface(ptr_game_sprite_sheet_surface);
	SDL_DestroyRenderer(ptr_renderer);
	SDL_DestroyWindow(ptr_window);
	SDL_Quit();
}


DrawComponent* GetDrawComponent(const char* name)
{
	DrawComponent* result = &draw_component_map.at(name);
	return result;
}

DrawComponent* GetBackgroundDrawComponent()
{
	return &background_draw_component;
}

DrawComponent* GetExplosionDrawComponent()
{
	return &explosion_draw_component;
}

void KeyPressed(const SDL_Event& event, SDL_Keycode key, KeyState* out_key_state)
{
	if (event.key.keysym.sym == key)
	{
		*out_key_state = KeyState::Pressed;
	}
}

void KeyReleased(const SDL_Event& event, SDL_Keycode key, KeyState* out_key_state)
{
	if (event.key.keysym.sym == key)
	{
		*out_key_state = KeyState::Released;
	}
}

void DrawSprite(void* draw_component, Rect* source_rect, I32 x, I32 y, U32 w, U32 h, F32 rotation)
{
	DrawComponent* ptr_draw_comp = static_cast<DrawComponent*>(draw_component);

	SDL_Rect draw = {
		static_cast<int>(x),
		static_cast<int>(y),
		static_cast<int>(w),
		static_cast<int>(h)
	};

	F32 half_w = w / 2;
	F32 half_h = h / 2;

	// Find middle of sprite for rotation, which is just width/height halfed divided by our scale.
	F32 rotation_x_origin = half_w;
	F32 rotation_y_origin = half_h;

	SDL_Point origin{ rotation_x_origin, rotation_y_origin };

	if (source_rect)
	{
		SDL_Rect source = {
			source_rect->X + half_w,
			source_rect->Y + half_h,
			source_rect->W,
			source_rect->H
		};

		

		SDL_RenderCopyEx(ptr_renderer, ptr_draw_comp->ptr_texture, &source, &draw, Degrees(rotation), &origin, SDL_RendererFlip::SDL_FLIP_NONE);
	}
	else
	{
		SDL_RenderCopyEx(ptr_renderer, ptr_draw_comp->ptr_texture, &ptr_draw_comp->source_rect, &draw, Degrees(rotation), &origin, SDL_RendererFlip::SDL_FLIP_NONE);
	}
}

#endif // !USE_SFML
