// *****************************************************************************
/*
	Please note that game is structure in a way, that makes it possible to replace SFML specific stuff with
	and other renderer stuff and that game_layer and anything related to actual gameplay and game code
	should not be referencing any third party libraries.

	To start a game simply call functions from SFML layer code in usual order for games
	// -- init
	// -- load assets
	// -- run
	// -- free resources
*/
// *****************************************************************************

// include emscripten header if compiling with it.
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif 

#include "game_defs.hpp"
#include "game_layer.hpp"

#if USE_SFML == 1

#include "sfml_layer.hpp"

void SFML_Game()
{
	// Setup platform and load assets 
	SFML_Init();

	// TODO: move assets to be loaded here ? 
	SFML_LoadAssets();

	/********************************* INITIALIZE GAME ENTITIES HERE *******************************************/


	// ************** BACKGROUND *********** //
	DrawComponent* background_draw_component = GetBackgroundDrawComponent();
	InitBackground(background_draw_component);

	// ************** PLAYER *********** //
	DrawComponent* player_draw_component = GetDrawComponent("playerShip1_blue");

	// width, height, radius 


#ifdef DEBUG
	player_draw_component->DEBUG_draw_collision_rect = true;
#endif 
	InitPlayer(player_draw_component,
		player_draw_component->sprite.getLocalBounds().width * SCALE_ASSETS,
		player_draw_component->sprite.getLocalBounds().height * SCALE_ASSETS,
		player_draw_component->sprite.getLocalBounds().height * SCALE_ASSETS / 2);


	// ************** ENEMY *********** //
	DrawComponent * enemy_draw_component = GetDrawComponent("enemyRed1");

#ifdef DEBUG
	enemy_draw_component->DEBUG_draw_collision_rect = true;
#endif 
	InitEnemies(enemy_draw_component,
		enemy_draw_component->sprite.getLocalBounds().width * SCALE_ASSETS,
		enemy_draw_component->sprite.getLocalBounds().height * SCALE_ASSETS,
		enemy_draw_component->sprite.getLocalBounds().height * SCALE_ASSETS / 2);

	/********************** PLAYER BULLETS ***************************/
	DrawComponent * player_bullet_draw_component = GetDrawComponent("laserBlue03");

	// width, height , radius 
	InitPlayerBullets(player_bullet_draw_component,
		player_bullet_draw_component->sprite.getGlobalBounds().width * SCALE_ASSETS,
		player_bullet_draw_component->sprite.getGlobalBounds().height * SCALE_ASSETS,
		player_bullet_draw_component->sprite.getGlobalBounds().height * SCALE_ASSETS / 2);

	/************************ ENEMY BULLETS ***************************/
	DrawComponent * enemy_bullet_draw_component = GetDrawComponent("laserRed03");

	InitEnemyBullets(enemy_bullet_draw_component,
		enemy_bullet_draw_component->sprite.getGlobalBounds().width * SCALE_ASSETS,
		enemy_bullet_draw_component->sprite.getGlobalBounds().height * SCALE_ASSETS,
		enemy_bullet_draw_component->sprite.getGlobalBounds().height * SCALE_ASSETS / 2);

	/************************ EXPLOSIONS ***************************/

	DrawComponent * explosion_draw_component = GetExplosionDrawComponent();

	InitExplosions(explosion_draw_component,
		explosion_draw_component->sprite.getGlobalBounds().width / 4,
		explosion_draw_component->sprite.getGlobalBounds().height / 4,
		4, 4, 64, 64);

	/***********************************************************************************************************/

	SFML_Run();

	SFML_Destroy();
}

#else 

#include "sdl_layer.hpp"

void SDL_Game()
{
	// Setup platform and load assets 
	SDL_Init();

	// TODO: move assets to be loaded here ? 
	SDL_LoadAssets();

	/********************************* INITIALIZE GAME ENTITIES HERE *******************************************/

	DrawComponent* background_draw_component = GetBackgroundDrawComponent();
	InitBackground(background_draw_component);

	DrawComponent* player_draw_component = GetDrawComponent("playerShip1_blue");

	// width, height, radius 
	U32 pw = player_draw_component->source_rect.w * SCALE_ASSETS;
	U32 ph = player_draw_component->source_rect.h * SCALE_ASSETS;
	U32 pr = player_draw_component->source_rect.h * SCALE_ASSETS / 2;
	InitPlayer(player_draw_component, pw, ph, pr);

	DrawComponent* enemy_draw_component = GetDrawComponent("enemyRed1");

	// width, height, radius 
	U32 ew = enemy_draw_component->source_rect.w * SCALE_ASSETS;
	U32 eh = enemy_draw_component->source_rect.h * SCALE_ASSETS;
	U32 er = enemy_draw_component->source_rect.h * SCALE_ASSETS / 2;
	InitEnemies(enemy_draw_component, ew, eh, er);

	/********************** PLAYER BULLETS ***************************/
	DrawComponent* player_bullet_draw_component = GetDrawComponent("laserBlue03");

	// width, height , radius 
	InitPlayerBullets(player_bullet_draw_component,
		player_bullet_draw_component->source_rect.w * SCALE_ASSETS,
		player_bullet_draw_component->source_rect.h * SCALE_ASSETS,
		player_bullet_draw_component->source_rect.h * SCALE_ASSETS / 2);

	/************************ ENEMY BULLETS ***************************/
	DrawComponent* enemy_bullet_draw_component = GetDrawComponent("laserRed03");

	InitEnemyBullets(enemy_bullet_draw_component,
		enemy_bullet_draw_component->source_rect.w * SCALE_ASSETS,
		enemy_bullet_draw_component->source_rect.h * SCALE_ASSETS,
		enemy_bullet_draw_component->source_rect.h * SCALE_ASSETS / 2);

	/************************ EXPLOSIONS ***************************/

	DrawComponent* explosion_draw_component = GetExplosionDrawComponent();

	InitExplosions(explosion_draw_component, explosion_draw_component->source_rect.w, explosion_draw_component->source_rect.h,
		4, 4, 64, 64);

	/***********************************************************************************************************/

	// when using emscripten, loop is done by requestAnimationFrame, this sets it up.
#if __EMSCRIPTEN__
	// https://emscripten.org/docs/api_reference/emscripten.h.html#id3
	emscripten_set_main_loop(SDL_Run, FPS, 1);
#else 
	SDL_Run();
#endif
	SDL_Destroy();
}

#endif

int main()
{
#if USE_SFML
	SFML_Game();
#else 
	SDL_Game();
#endif

	return 0;
}