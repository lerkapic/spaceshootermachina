#pragma once
#include <cstdint>

#ifndef GAME_DEFS_H

#define GAME_DEFS_H

/*
	All constants and preprocessor definitions for a game,
	stuff such as width and height of windows, framerate etc...

	Carefull all definitions are pretty much tuned to be in scale with game assets.
	For example background is 256 256 and windows width and height is multiple of that.
*/

// If USE_SFML uses SFML, otherwise defaults to SDL2
#define USE_SFML 1

#define DEBUG 

#define PATH_TO_GAME_ART_SHEET "art/SpaceShooterRedux/Spritesheet/"
#define PATH_TO_BACKGROUND "art/SpaceShooterRedux/Backgrounds/"
#define PATH_TO_EXPLOSIONS "art/Explosion/"

#define BACKGROUND_WIDTH 256
#define BACKGROUND_HEIGHT 256
#define BACKGROUND_SCALE_X 4
#define BACKGROUND_SCALE_Y 3
#define BACKGROUND_SCROLL_SPEED 0.1f

#define WINDOW_WIDTH (BACKGROUND_WIDTH * BACKGROUND_SCALE_X)
#define WINDOW_HEIGHT (BACKGROUND_HEIGHT * BACKGROUND_SCALE_Y)

// Frame rate and frame time in ms.
#define FPS 60
#define MS_PER_FRAME (1000 / FPS)

// Player and enemy physics setup
#define PLAYER_ACCELERATION 0.0025f
#define PLAYER_MAX_VELOCITY_MAGNITUDE 0.4f
#define PLAYER_FRICTION_FACTOR 0.995f
#define PLAYER_ROTATION_SPEED 0.005f
#define ENEMY_MAX_ACCELERATION_MAGNITUDE 1.0f
#define ENEMY_MAX_VELOCITY_MAGNITUDE 0.2f

#define PLAYER_BULLET_MAX_VELOCITY_MAGNITUDE 0.9f
#define PLAYER_BULLET_SPEED -2.0f // needs to go up, therefore negative
#define PLAYER_SHOOTING_COOLDOWN_MS 1000.0f // in miliseconds


#define ENEMY_BULLET_MAX_VELOCITY_MAGNITUDE 0.9f
#define ENEMY_BULLET_SPEED 1.5f

// random between, to make it more engaging, with randomness.
#define ENEMY_SHOOTING_COOLDOWN_MS_MIN 1000.0f 
#define ENEMY_SHOOTING_COOLDOWN_MS_MAX 3000.0f 

#define SCALE_ASSETS 0.5f

#define ANIMATION_SPEED 20

// How often to spawn enemies in miliseconds
#define SPAWN_EVERY_MS 2000

#define ArrayCount(arr) sizeof(arr)/sizeof(arr[0])

typedef float F32;
typedef uint32_t U32;
typedef int32_t I32;

struct Rect
{
	I32 X;
	I32 Y;
	I32 W;
	I32 H;
};

#endif // !GAME_DEFS_H
