emcc -Ilibs\rapidxml-1.13^
 main.cpp game_layer.cpp game_physics.cpp sdl_layer.cpp^
 -s USE_SDL=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS=["png"]^
 -o wasm_build/index.html^
 --embed-file art\SpaceShooterRedux\Backgrounds\purple.png --embed-file art\SpaceShooterRedux\Spritesheet --embed-file art\Explosion\exp.png^ 
 --use-preload-plugins