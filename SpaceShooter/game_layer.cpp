#include "game_layer.hpp"
#include "game_defs.hpp"
#include "game_physics.hpp"
#include "bones_behaviour_systems.hpp"
#include <stdlib.h>

Camera camera;
Player player;
// Inital pool of enemies , still prototyping for now.
Enemy enemies[50];
Bullet enemy_bullets[30];
Bullet player_bullets[30];

// Backgrounds should just exist around player, and be created on demand around him based on player position and direction.
Background backgrounds[9];
Explosion explosions[20];

void SpawnEnemy();
void SpawnEnemyBullet(const Enemy& enemy);
void SpawnExplosion(Vec2f position);
void UpdateEnemies(F32 dt);
void UpdateBullets(F32 dt);
void UpdateExplosions(F32 dt);
void UpdateAndShootPlayerBullets(const KeyboardEvents& key_events, F32 dt);
bool CheckCircleCollision(const Player& player, const Enemy& enemy);

void SyncBackgroundsToPlayer(const Player& player);

/// <summary>
/// Check if two rectangles are overlapping
/// </summary>
bool AxisAlignedBoundingBoxCollision(Rect a, Rect b);
bool AxisAlignedBoundingBoxCollision(const Player& player, const Enemy& enemy);
bool AxisAlignedBoundingBoxCollision(const Enemy& enemy, const Bullet& player_bullet);
bool AxisAlignedBoundingBoxCollision(const Player& player, const Bullet& enemy_bullet);
//void ScrollBackgrounds(F32 dt);


/// <summary>
/// Is entry out of bounds.
/// </summary>
bool OutOfGameWorldBounds(const Transform& entry);

inline F32 GetEnemyShootingCooldown()
{
	F32 result = Random(ENEMY_SHOOTING_COOLDOWN_MS_MIN, ENEMY_SHOOTING_COOLDOWN_MS_MAX);
	return result;
}

struct Game
{
	float spawn_timer;
} game;

void InitPlayer(void* drawable, F32 size_x, F32 size_y, F32 radius)
{
	camera.Offset = Vec2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
	player.Position = Vec2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
	player.Size.X = size_x;
	player.Size.Y = size_y;
	player.Radius = radius;
	player.Angle = 0.0f;
	player.Drawable = drawable;
}

void InitEnemies(void* drawable, F32 size_x, F32 size_y, F32 radius)
{
	for (size_t i = 0; i < ArrayCount(enemies); i++)
	{
		enemies[i].Drawable = drawable;

		// temp 
		enemies[i].Position.X = rand() % WINDOW_WIDTH;
		enemies[i].Position.Y = rand() % WINDOW_HEIGHT;
		enemies[i].Size.X = size_x;
		enemies[i].Size.Y = size_y;
		enemies[i].Radius = radius;
		enemies[i].Active = false;
		enemies[i].ShootingCooldown = GetEnemyShootingCooldown();
	}
}

void InitPlayerBullets(void* drawable, F32 size_x, F32 size_y, F32 radius)
{
	for (size_t i = 0; i < ArrayCount(player_bullets); i++)
	{
		player_bullets[i].Drawable = drawable;
		player_bullets[i].Size.X = size_x;
		player_bullets[i].Size.Y = size_y;
		player_bullets[i].Radius = radius;
		player_bullets[i].Active = false;
	}
}

void InitEnemyBullets(void* drawable, F32 size_x, F32 size_y, F32 radius)
{
	for (size_t i = 0; i < ArrayCount(enemy_bullets); i++)
	{
		enemy_bullets[i].Drawable = drawable;
		enemy_bullets[i].Size.X = size_x;
		enemy_bullets[i].Size.Y = size_y;
		enemy_bullets[i].Radius = radius;
		enemy_bullets[i].Velocity = Vec2f(0, ENEMY_BULLET_SPEED);
		enemy_bullets[i].Active = false;
	}
}

void InitBackground(void* drawable)
{
	size_t i = 0;
	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			Background& background = backgrounds[i++];
			background.Drawable = drawable;
			background.Position.X = 
			background.Size.X = BACKGROUND_WIDTH * BACKGROUND_SCALE_X;
			background.Size.Y = BACKGROUND_HEIGHT * BACKGROUND_SCALE_Y;
			background.Position.X = background.Size.X * x;
			background.Position.Y = background.Size.Y * y;
		}
	}
}

void InitExplosions(void* drawable, F32 size_x, F32 size_y, U32 TotalAnimationRows, U32 TotalAnimationColumns, U32 real_size_x, U32 real_size_y)
{
	for (size_t i = 0; i < ArrayCount(explosions); i++)
	{
		explosions[i].Drawable = drawable;
		explosions[i].Size.X = size_x;
		explosions[i].Size.Y = size_y;
		explosions[i].RealSize = Vec2f(real_size_x, real_size_y);
		explosions[i].TotalAnimationRows = TotalAnimationRows;
		explosions[i].TotalAnimationColumns = TotalAnimationColumns;
		explosions[i].AnimationNextStepCooldown = ANIMATION_SPEED;
	}
}

GAME_UPDATE(UpdateGame)
{
	float dt = game_update_input.DeltaTime;

	game.spawn_timer += dt;

	if (game.spawn_timer > SPAWN_EVERY_MS)
	{
		SpawnEnemy();
		game.spawn_timer = 0.0f;
	}

	// for key events one can simply use bits algebra

	const KeyboardEvents& key_events = game_update_input.KeyboardEvents;

	// for acceleration and physics see https://natureofcode.com/book/chapter-2-forces/ , but note that it's in JavaScript therefore there it no dt
	Vec2f acceleration = player.Acceleration;
	acceleration.SetLengthToZero();

	if (key_events.Left_KeyDown == KeyState::Pressed || key_events.A_KeyDown == KeyState::Pressed)
	{
		player.Angle += -PLAYER_ROTATION_SPEED * dt;
	}
	if (key_events.Right_KeyDown == KeyState::Pressed || key_events.D_KeyDown == KeyState::Pressed)
	{
		player.Angle += PLAYER_ROTATION_SPEED * dt;
	}

	if (key_events.Up_KeyDown == KeyState::Pressed || key_events.W_KeyDown == KeyState::Pressed)
	{
		Vec2f rotation_vector = Vec2f::FromDirection(player.Angle);
		acceleration.X = rotation_vector.X;
		acceleration.Y = rotation_vector.Y;
	}
	

	// TODO: Handle diagonal movement 
	acceleration.Normalize();
	UpdateMotion(player.Position, player.Velocity, acceleration, dt, PLAYER_FRICTION_FACTOR);
	player.Velocity.ClampToMaxMagnitude(PLAYER_MAX_VELOCITY_MAGNITUDE);

	// TODO: smooth camera to player, this is bit rough.
	camera.Position = player.Position;

	UpdateAndShootPlayerBullets(key_events, dt);


	UpdateEnemies(dt);
	UpdateBullets(dt);
	UpdateExplosions(dt);
	//ScrollBackgrounds(dt);

	SyncBackgroundsToPlayer(player);
}

GAME_RENDER(RenderGame)
{
	// Entities must be moved to screen space from camera space, so use this to find position in screen space.
	Vec2f camera_position_offset = camera.Position - camera.Offset;

	for (size_t i = 0; i < ArrayCount(backgrounds); i++)
	{
		const Background& bg = backgrounds[i];

		Vec2f position = bg.Position - camera_position_offset;

		cb_draw_function(bg.Drawable, nullptr, position.X, position.Y, bg.Size.X, bg.Size.Y, 0.0f);
	}



	// Player
	Vec2f player_position = player.Position - camera_position_offset;
	cb_draw_function(player.Drawable, nullptr, player_position.X, player_position.Y, player.Size.X, player.Size.Y, player.Angle + HALF_PI);

	// Enemies
	for (size_t i = 0; i < ArrayCount(enemies); i++)
	{
		const Enemy& enemy = enemies[i];

		if (!enemy.Active) continue;


		Vec2f enemy_position = enemy.Position - camera_position_offset;
		cb_draw_function(enemy.Drawable, nullptr, enemy_position.X, enemy_position.Y, enemy.Size.X, enemy.Size.Y, enemy.Velocity.Direction() - HALF_PI);
	}

	for (size_t i = 0; i < ArrayCount(player_bullets); i++)
	{
		const Bullet& bullet = player_bullets[i];

		if (!bullet.Active) continue;

		cb_draw_function(bullet.Drawable, nullptr, bullet.Position.X, bullet.Position.Y, bullet.Size.X, bullet.Size.Y, 0.0f);
	}

	for (size_t i = 0; i < ArrayCount(enemy_bullets); i++)
	{
		const Bullet& bullet = enemy_bullets[i];

		if (!bullet.Active) continue;

		cb_draw_function(bullet.Drawable, nullptr, bullet.Position.X, bullet.Position.Y, bullet.Size.X, bullet.Size.Y, 0.0f);
	}

	for (size_t i = 0; i < ArrayCount(explosions); i++)
	{
		const Explosion& exp = explosions[i];

		if (!exp.Active) continue;

		Rect source_rect = {
			static_cast<I32>(exp.RealSize.X * exp.AnimationColumn),
			static_cast<I32>(exp.RealSize.Y * exp.AnimationRow),
			static_cast<I32>(exp.RealSize.X),
			static_cast<I32>(exp.RealSize.Y)
		};

		cb_draw_function(exp.Drawable, &source_rect, exp.Position.X, exp.Position.Y, exp.Size.X, exp.Size.Y, 0.0f);
	}
}

/*
	Sets enemy to active and restarts it's position to be on top of screen
*/
void SpawnEnemy()
{
	for (size_t i = 0; i < ArrayCount(enemies); i++)
	{
		Enemy& enemy = enemies[i];
		if (enemy.Active == false)
		{
			enemy.Active = true;

			// Create enemy out of current screen bounds, that is it should be player position + half screen width or minus half screen width for horizontal
			// and player position + half height - half height for vertical.

			I32 screen_side = Random(0, 4);

			// Some random position. 
			// NOTE: spawn_x or spawn_y is bound later.
			F32 screen_left = player.Position.X - WINDOW_WIDTH / 2;
			F32 screen_right = player.Position.X + WINDOW_WIDTH / 2;
			F32 screen_top = player.Position.Y - WINDOW_HEIGHT / 2;
			F32 screen_bottom = player.Position.Y + WINDOW_HEIGHT / 2;
			F32 spawn_x = Random(screen_left, screen_right);
			F32 spawn_y = Random(screen_top, screen_bottom);

			// TODO: improve offsets
			
			if (screen_side == 0)
			{
				spawn_y = screen_top;
			}
			else if (screen_side == 1)
			{
				spawn_y = screen_bottom;
			}
			else if (screen_side == 2)
			{
				spawn_x = screen_left;
			}
			else if (screen_side == 3)
			{
				spawn_x = screen_right;
			}
			else
			{
				spawn_x = 0;
			}

			U32 rough_player_width = enemy.Radius * 2;
			enemy.Position.X = spawn_x;
			enemy.Position.Y = spawn_y;
	
			break;
		}
	}
}

/*
	Update the enemies game logic.
*/
void UpdateEnemies(F32 dt)
{
	for (size_t i = 0; i < ArrayCount(enemies); i++)
	{
		Enemy& enemy = enemies[i];

		if (!enemy.Active) continue;

		if (AxisAlignedBoundingBoxCollision(player, enemy))
		{
			enemy.Active = false;
			SpawnExplosion(enemy.Position);
			SpawnExplosion(player.Position);
			continue;
		}

		F32 enemy_direction = enemy.Velocity.Direction();
		// Direction of enemy looking towards a player.
		F32 player_enemy_direction = (player.Position - enemy.Position).Direction();

		// If player is not looking directly or close enough to player seek it ( to avoid small shimmering and precision issues) 
		if (Abs(enemy_direction - player_enemy_direction) > 0.15f)
		{
			SeekTarget(enemy.Position, enemy.Velocity, player.Position, dt, enemy.Acceleration);
		}

		enemy.Acceleration.ClampToMaxMagnitude(ENEMY_MAX_ACCELERATION_MAGNITUDE);
		UpdateMotion(enemy.Position, enemy.Velocity, enemy.Acceleration, dt, 1.0);
		enemy.Velocity.ClampToMaxMagnitude(ENEMY_MAX_VELOCITY_MAGNITUDE);


		enemy.ShootingCooldown -= dt;
		if (enemy.ShootingCooldown < 0.0f)
		{
			SpawnEnemyBullet(enemy);
			enemy.ShootingCooldown = GetEnemyShootingCooldown();
		}

		// if enemy is out of screen bounds, it should be inactive.
		if (OutOfGameWorldBounds(enemy))
		{
			enemy.Active = false;
			continue;
		}
	}
}

bool CheckCircleCollision(const Player& player, const Enemy& enemy)
{
	// find center positions to do circle collision
	Vec2f player_center;
	player_center.X = player.Position.X + player.Size.X * 0.5f;
	player_center.Y = player.Position.Y + player.Size.Y * 0.5f;

	Vec2f enemy_center;
	enemy_center.X = enemy.Position.X + enemy.Size.X * 0.5f;
	enemy_center.Y = enemy.Position.Y + enemy.Size.Y * 0.5f;

	Vec2f delta_vec = player_center - enemy_center;
	F32 distance_sq = delta_vec.LengthSquared();

	// Formula is d < player_r + enemy_r ( but they are supposed to be same size in code so whatever for now )
	F32 radius_sum = player.Radius + enemy.Radius;

	// distance is squared, therefore square radius sum
	bool result = distance_sq < (radius_sum* radius_sum);
	return result;
}

bool AxisAlignedBoundingBoxCollision(const Player& player, const Enemy& enemy)
{
	Rect a = {
		static_cast<I32>(player.Position.X),
		static_cast<I32>(player.Position.Y),
		static_cast<I32>(player.Size.X),
		static_cast<I32>(player.Size.Y)
	};

	Rect b = {
		static_cast<I32>(enemy.Position.X),
		static_cast<I32>(enemy.Position.Y),
		static_cast<I32>(enemy.Size.X),
		static_cast<I32>(enemy.Size.Y)
	};

	bool result = AxisAlignedBoundingBoxCollision(a, b);
	return result;
}


bool AxisAlignedBoundingBoxCollision(const Enemy& enemy, const Bullet& player_bullet)
{
	Rect a = {
		static_cast<I32>(player_bullet.Position.X),
		static_cast<I32>(player_bullet.Position.Y),
		static_cast<I32>(player_bullet.Size.X),
		static_cast<I32>(player_bullet.Size.Y)
	};

	Rect b = {
		static_cast<I32>(enemy.Position.X),
		static_cast<I32>(enemy.Position.Y),
		static_cast<I32>(enemy.Size.X),
		static_cast<I32>(enemy.Size.Y)
	};

	bool result = AxisAlignedBoundingBoxCollision(a, b);
	return result;
}

bool AxisAlignedBoundingBoxCollision(const Player& player, const Bullet& enemy_bullet)
{
	Rect a = {
		static_cast<I32>(player.Position.X),
		static_cast<I32>(player.Position.Y),
		static_cast<I32>(player.Size.X),
		static_cast<I32>(player.Size.Y)
	};

	Rect b = {
		static_cast<I32>(enemy_bullet.Position.X),
		static_cast<I32>(enemy_bullet.Position.Y),
		static_cast<I32>(enemy_bullet.Size.X),
		static_cast<I32>(enemy_bullet.Size.Y)
	};


	bool result = AxisAlignedBoundingBoxCollision(a, b);
	return result;
}

void SpawnEnemyBullet(const Enemy& enemy)
{
	for (U32 i = 0; i < ArrayCount(enemy_bullets); i++)
	{
		Bullet& bullet = enemy_bullets[i];
		if (!bullet.Active)
		{
			bullet.Active = true;
			bullet.Position.X = enemy.Position.X + enemy.Size.X / 2;
			bullet.Position.Y = enemy.Position.Y + enemy.Size.Y / 2;
			bullet.Acceleration = Vec2f(0, ENEMY_MAX_ACCELERATION_MAGNITUDE * 2.0f);
			break;
		}
	}
}

void UpdateBullets(F32 dt)
{
	for (size_t i = 0; i < ArrayCount(player_bullets); i++)
	{
		Bullet& player_bullet = player_bullets[i];

		if (!player_bullet.Active) continue;

		// Bullet goes from bottom of screen up, therefore when out of bounds, set it to inactive
		if (OutOfGameWorldBounds(player_bullet))
		{
			player_bullet.Active = false;
		}
	}

	for (size_t i = 0; i < ArrayCount(enemy_bullets); i++)
	{
		Bullet& enemy_bullet = enemy_bullets[i];

		if (!enemy_bullet.Active) continue;

		UpdateMotion(enemy_bullet.Position, enemy_bullet.Velocity, enemy_bullet.Acceleration, dt);
		enemy_bullet.Velocity.SetMagnitude(ENEMY_BULLET_MAX_VELOCITY_MAGNITUDE);

		if (AxisAlignedBoundingBoxCollision(player, enemy_bullet))
		{
			SpawnExplosion(player.Position);
		}

		if (OutOfGameWorldBounds(enemy_bullet))
		{
			enemy_bullet.Active = false;
		}
	}
}

void UpdateAndShootPlayerBullets(const KeyboardEvents& key_events, F32 dt)
{
	// Cooldown for shooting
	player.ShootingCooldown -= dt;

	// Use space key as shooting key, and find first bullet to shoot.
	if (key_events.Space_KeyDown == KeyState::Pressed)
	{
		// cooldown must be lower then 0
		if (player.ShootingCooldown < 0.0f)
		{
			// find first inactive bullet and set it's position, velocity and active code
			for (U32 i = 0; i < ArrayCount(player_bullets); i++)
			{
				Bullet& player_bullet = player_bullets[i];
				if (!player_bullet.Active)
				{
					player_bullet.Active = true;
					player_bullet.Velocity.Y = -1;
					player_bullet.Position.X = player.Position.X + player.Size.X / 2;
					player_bullet.Position.Y = player.Position.Y - (player.Size.Y / 2);
					break;
				}
			}

			// reset player shooting cooldown
			if (player.ShootingCooldown)
			{
				player.ShootingCooldown = PLAYER_SHOOTING_COOLDOWN_MS;
			}
		}
	}

	for (size_t i = 0; i < ArrayCount(player_bullets); i++)
	{
		if (player_bullets[i].Active)
		{
			UpdateMotion(player_bullets[i].Position, player_bullets[i].Velocity, player_bullets[i].Acceleration, dt, 1.0);
			player_bullets[i].Velocity.ClampToMaxMagnitude(PLAYER_BULLET_MAX_VELOCITY_MAGNITUDE);

			// check bullet against all enemies 
			for (size_t j = 0; j < ArrayCount(enemy_bullets); j++)
			{
				if (enemies[j].Active)
				{
					// bullet enemy collision
					bool player_bullet_collides_enemy = AxisAlignedBoundingBoxCollision(enemies[j], player_bullets[i]);
					if (player_bullet_collides_enemy)
					{
						enemies[j].Active = false;
						player_bullets[i].Active = false;
						SpawnExplosion(enemies[j].Position);
					}
				}
			}

			// if player bullet is out of bounds, deactivate it 
			// TODO: check if inside screen bounds instead if top is passed.
			if (player_bullets[i].Position.Y - player_bullets[i].Size.Y < 0)
			{
				player_bullets[i].Active = false;
			}
		}
	}
}

void SpawnExplosion(Vec2f position)
{
	for (size_t i = 0; i < ArrayCount(explosions); i++)
	{
		Explosion& exp = explosions[i];
		if (!exp.Active)
		{
			exp.Active = true;
			// reset animation
			exp.AnimationRow = 0;
			exp.AnimationColumn = 0;
			exp.Position = position;
			break;
		}
	}
}

void UpdateExplosions(F32 dt)
{
	for (size_t i = 0; i < ArrayCount(explosions); i++)
	{
		Explosion& exp = explosions[i];
		if (exp.Active)
		{
			// explosion has an animation step cooldown, when expired, move to next part of animation
			exp.AnimationNextStepCooldown -= dt;
			if (exp.AnimationNextStepCooldown <= 0.0f)
			{
				// go through rows and columns of explosion to play it.
				// assumes explosions are moving columns first, then rows.
				// NOTE: That columns cannot go above total - 1
				if (exp.AnimationColumn < exp.TotalAnimationColumns - 1)
				{
					exp.AnimationColumn++;
				}
				else
				{
					exp.AnimationRow++;
					exp.AnimationColumn = 0;
				}

				// If finished, when there are not rows to play
				if (exp.AnimationRow >= exp.TotalAnimationRows - 1)
				{
					exp.Active = false;
					exp.AnimationRow = 0;
					exp.AnimationColumn = 0;
				}

				exp.AnimationNextStepCooldown = ANIMATION_SPEED;
			}

		}
	}
}

bool OutOfGameWorldBounds(const Transform& entry)
{
	// Not sure what the world bounds are right now currently or what the playable area is , note that some stuff can go a bit out of screen, enemies, different bullets etc,
	// therefore offset 

	// TODO: Define world bounds
	bool result = entry.Position.X < -entry.Size.X * 5.0f
		|| entry.Position.X > WINDOW_WIDTH + 100.0f
		|| entry.Position.Y < -entry.Size.Y * 5.0f
		|| entry.Position.Y > WINDOW_HEIGHT + 100.0f;

	return result;
}

void SyncBackgroundsToPlayer(const Player& player)
{
	// World index just tells us where middle backgrund is currently in world 
	// ( background is 9x9 grid and it's done so that player is always in center of grid, therfore needs to be realinged )
	Point2i backgrond_world_source_index = {
		static_cast<I32>(backgrounds[4].Position.X) / WINDOW_WIDTH,
		static_cast<I32>(backgrounds[4].Position.Y) / WINDOW_HEIGHT
	};

	I32 player_world_index_x = static_cast<I32>(player.Position.X) / WINDOW_WIDTH;
	// offset by -1 if it's less then 0, due to multiplication difference	-player_x / WINDOW_WIDTH gives index 0 for values below WINDOW_WIDTH which is wrong
	if (player.Position.X < 0)
	{
		player_world_index_x -= 1;
	}


	I32 player_world_index_y = static_cast<I32>(player.Position.Y) / WINDOW_HEIGHT;
	// Same as for x, but for y instead.
	if (player.Position.Y < 0)
	{
		player_world_index_y -= 1;
	}

	// sync if any of indexes is not the same
	if (player_world_index_x != backgrond_world_source_index.X
		|| player_world_index_y != backgrond_world_source_index.Y)
	{
		// move backgrounds to grid around player.
		size_t i = 0;
		for (int x = -1; x <= 1; x++)
		{
			for (int y = -1; y <= 1; y++)
			{
				// Set correct grid positions
				Background& background = backgrounds[i++];
				background.Position.X = background.Size.X * (player_world_index_x + x);
				background.Position.Y = background.Size.Y * (player_world_index_y + y);
			}
		}
	}
}