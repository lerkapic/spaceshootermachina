#include "game_physics.hpp"


void UpdateMotion(Vec2f& in_out_current_position, Vec2f& in_out_current_velocity, Vec2f acceleration, F32 dt, F32 friction_factor)
{
	acceleration *= PLAYER_ACCELERATION;

	// current velocity + new velocity, where formula for new velocity is ( v + t ) or velocity * time or delta_time
	Vec2f new_velocity = acceleration * dt;
	in_out_current_velocity += new_velocity;

	// not excatly accurate, but whatever. Friction must be close to 1 , but less then 1 in order to work properly, values such as 0.995f ,
	// because on each frames, it lowers it a bit do velocity of length 1 , becomes velocity of length 0.995f on next frame , then bit less on next etc... 
	in_out_current_velocity *= friction_factor;

	in_out_current_position = in_out_current_position + (in_out_current_velocity * dt);
}


void UpdateMotion(Vec2f& in_out_current_position, Vec2f& in_out_current_velocity, Vec2f acceleration, F32 dt)
{
	UpdateMotion(in_out_current_position, in_out_current_velocity, acceleration, dt, 1.0f);
}

bool AxisAlignedBoundingBoxCollision(Rect a, Rect b)
{
	bool result = a.X < b.X + b.W &&
		a.X + a.W > b.X &&
		a.Y < b.Y + b.H &&
		a.Y + a.H > b.Y;

	return result;
}