### Game project as part of course for https://machina.academy/c-programiranje

This project is simple SpaceShooter ( let's see how far it goes ) simply made for educational purposes, feel free to use this code as you please,
and can be considered as MIT, until licensce is here.

This project is imagined as exercise in C++ and game arhitecture, so it's composed of platform independent game layer and platform layer ( right now it uses SFML, SDL tested on Windows with x64 build, and build to WebAssembly with usage of EMSCRIPTEN ).

Part of this project will be eventually pulled out and created as separate "bones" library just for purpose of reusing it for any other project. 

#### Compilation for Windows 

I'm using Visual Studio Community 2019 , so I highly recommend that one. Simply load the solution and it should work for x64 out of box.

#### Compilation for browser

For this one you would need https://emscripten.org/

This is tested on Windows machine.

Once the emsdk (emscripten sdk) is installed go to containg folder and start 'emcmdprompt.bat'. 
Withing command line navigate to project folder and run withing command line 'wasm_build'.
Build for web is located in 'wasm_build' folder.
One needs to start simple http-server and viola, it should work.

#### ART

Space Shooter https://opengameart.org/content/space-shooter-redux
Explosions https://opengameart.org/content/explosion

